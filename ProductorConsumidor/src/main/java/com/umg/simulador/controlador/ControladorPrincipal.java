package com.umg.simulador.controlador;

import com.umg.simulador.vista.Vista;
import javax.swing.JLabel;

/**
 *
 * @author jaasiel17
 */
public class ControladorPrincipal {

    private Vista v;
    private Semaforo semaforo;
    private Buffer buffer;
    private Productor productor;
    private Consumidor consumidor;
    private Consumidor consumidor2;

    public ControladorPrincipal(Vista v) {

        this.v = v;
        this.semaforo = new Semaforo(v.semaforoP, v.semaforoC);
        this.buffer = new Buffer(v.pnlBuffer, v.barraBuffer);
        this.productor = new Productor(buffer, semaforo, v.productor, v.lblEstado);
        this.consumidor = new Consumidor(buffer, semaforo, v.consumidor1, new JLabel[]{v.lblEstado1, v.lblEstado3}, 1);
        this.consumidor2 = new Consumidor(buffer, semaforo, v.consumidor2, new JLabel[]{v.lblEstado2, v.lblEstado3}, 2);

        v.btnPlay.addActionListener(((e) -> {
            play();
        }));

        v.btnStop.addActionListener(((e) -> {
            stop();
        }));

        v.setLocationRelativeTo(null);
        v.setVisible(true);
        v.btnStop.setEnabled(false);

    }

    private void play() {

        productor.start();
        consumidor2.start();
        consumidor.start();
        v.btnPlay.setEnabled(false);
        v.btnStop.setEnabled(true);

    }

    private void stop() {

        productor.stop();
        consumidor2.stop();
        consumidor.stop();

        this.semaforo = new Semaforo(v.semaforoP, v.semaforoC);
        this.buffer = new Buffer(v.pnlBuffer, v.barraBuffer);
        this.productor = new Productor(buffer, semaforo, v.productor, v.lblEstado);
        this.consumidor = new Consumidor(buffer, semaforo, v.consumidor1, new JLabel[]{v.lblEstado1, v.lblEstado3}, 1);
        this.consumidor2 = new Consumidor(buffer, semaforo, v.consumidor2, new JLabel[]{v.lblEstado2, v.lblEstado3}, 2);

        v.btnStop.setEnabled(false);
        v.btnPlay.setEnabled(true);

    }

}
