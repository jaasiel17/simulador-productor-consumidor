package com.umg.simulador.controlador;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 *
 * @author jaasiel17
 */
public class Consumidor extends Thread {

    private final Buffer b;
    private final Semaforo s;
    private final JLabel imgConsumidor;
    private final JLabel imgEstado;
    private final JLabel imgEstado2;
    private final int consumidor;

    public Consumidor(Buffer b, Semaforo s, JLabel imgConsumidor, JLabel[] imgEstado, int consumidor) {
        this.b = b;
        this.s = s;
        this.imgConsumidor = imgConsumidor;
        this.imgEstado = imgEstado[0];
        this.imgEstado2 = imgEstado[1];
        this.consumidor = consumidor;
        SwingUtilities.invokeLater(() -> {

            if (consumidor == 1) {
                this.imgConsumidor.setLocation(940, 100);
            } else {
                this.imgConsumidor.setLocation(950, 520);
            }

            this.imgEstado2.setVisible(false);
            this.imgEstado.setVisible(true);
        });

    }

    @Override

    public void run() {
        while (true) {
            comprarMueble();
        }
    }

    private void comprarMueble() {
        //bloque de codigo sincronizado con los otros hilos
        synchronized (s.lock) {

            if (b.size() > 0) { // Se carga con un fichero wav

                s.alternar(Semaforo.CONSUMIR);// cambiar color de semaforo
                SwingUtilities.invokeLater(() -> {

                    imgEstado.setVisible(false);

                });

                animation1();

                try {
                    sleep((long) ((Math.random() * ((5000 - 300) + 1)) + 300));
                } catch (InterruptedException ex) {
                    JOptionPane.showMessageDialog(null, "Error sleep en Consumidor: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }

                b.remove();
                System.out.println("Quitando mueble....");

                animation2();

                s.lock.notifyAll();// despertar a los hilos

                SwingUtilities.invokeLater(() -> {

                    imgEstado.setVisible(true);
                });
                try {
                    s.lock.wait();//ahora poner a dormir al consumidor
                } catch (InterruptedException ex) {
                    JOptionPane.showMessageDialog(null, "Error poniendo Consumidor en espera: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }

            } else {// si esta vacio, se despierta al Productor para que cree mas muebles

                s.lock.notifyAll();// despertar a los hilos, y por ende despertara el Productor
                SwingUtilities.invokeLater(() -> {

                    imgEstado.setVisible(true);
                });
                try {
                    s.lock.wait();//ahora poner a dormir al consumidor
                } catch (InterruptedException ex) {
                    JOptionPane.showMessageDialog(null, "Error poniendo Consumidor en espera: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }

            }
        }
    }

    private void animation1() {
        int direccion = (consumidor == 1) ? 1 : -1;

        int x = imgConsumidor.getLocation().x;
        int y = imgConsumidor.getLocation().y;

        for (int i = 0; i < 210; i++) {

            y += direccion;

            imgConsumidor.setLocation(x, y);

            try {

                sleep(6);//
            } catch (InterruptedException ex) {
                JOptionPane.showMessageDialog(null, "Error sleep en Consumidor: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }

        }

        SwingUtilities.invokeLater(() -> {

            imgEstado2.setVisible(true);
        });

    }

    private void animation2() {

        SwingUtilities.invokeLater(() -> {

            imgEstado2.setVisible(false);
        });
        int direccion = (consumidor == 1) ? 1 : -1;

        int x = imgConsumidor.getLocation().x;
        int y = imgConsumidor.getLocation().y;

        for (int i = 0; i < 210; i++) {

            y -= direccion;

            imgConsumidor.setLocation(x, y);

            try {

                sleep(6);//
            } catch (InterruptedException ex) {
                JOptionPane.showMessageDialog(null, "Error sleep en Consumidor: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }

        }
    }

}
