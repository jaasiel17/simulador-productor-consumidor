package com.umg.simulador.controlador;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

/**
 *
 * @author jaasiel17
 */
public class Buffer {

    private final JPanel pnlBuffer;
    private final JProgressBar barra;

    public Buffer(JPanel pnlbuffer, JProgressBar barra) {
        this.pnlBuffer = pnlbuffer;
        this.barra = barra;

        SwingUtilities.invokeLater(() -> {
            this.pnlBuffer.removeAll();
            this.pnlBuffer.revalidate();
            this.pnlBuffer.repaint();
            this.barra.setValue(0);
        });

    }

    // agregar un jlabel al panel que simula el buffer
    public void add(JLabel producto) {

        SwingUtilities.invokeLater(() -> {
            if (pnlBuffer.getComponentCount() < 4) {

                try {
                    URL defaultSound = getClass().getResource("/Sonidos/Sonido4.wav");
                    // getClass().getSy.getResource("/images/ads/WindowsNavigationStart.wav");
                    File soundFile = new File(defaultSound.toURI());
                    AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(soundFile);
                    Clip clip = AudioSystem.getClip();
                    clip.open(audioInputStream);
                    clip.start();
                } catch (IOException | URISyntaxException | LineUnavailableException | UnsupportedAudioFileException ex) {
                    JOptionPane.showMessageDialog(null, "Error ejecutando audio del buffer: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }

                pnlBuffer.add(producto);
                pnlBuffer.revalidate();
                pnlBuffer.repaint();
                barra.setValue(pnlBuffer.getComponentCount());
            }
        });

    }

    // remueve el ultimo componente del buffer
    public void remove() {

        SwingUtilities.invokeLater(() -> {

            try {
                URL defaultSound = getClass().getResource("/Sonidos/Sonido7.wav");
                // getClass().getSy.getResource("/images/ads/WindowsNavigationStart.wav");
                File soundFile = new File(defaultSound.toURI());
                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(soundFile);
                Clip clip = AudioSystem.getClip();
                clip.open(audioInputStream);
                clip.start();
            } catch (IOException | URISyntaxException | LineUnavailableException | UnsupportedAudioFileException ex) {
                JOptionPane.showMessageDialog(null, "Error ejecutando audio del buffer: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }

            try {
                this.pnlBuffer.remove(pnlBuffer.getComponentCount() - 1);
                pnlBuffer.revalidate();
                pnlBuffer.repaint();
                barra.setValue(pnlBuffer.getComponentCount());

            } catch (ArrayIndexOutOfBoundsException e) {
                JOptionPane.showMessageDialog(null, "Error removiendo objeto del buffer: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }

        });

    }

    //devuelve el numero de componentes en el panel -> tamanio de buffer
    public int size() {
        return pnlBuffer.getComponentCount();
    }
}
