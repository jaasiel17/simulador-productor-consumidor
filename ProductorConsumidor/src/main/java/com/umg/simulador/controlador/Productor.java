package com.umg.simulador.controlador;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 *
 * @author jaasiel17
 */
public class Productor extends Thread {

    private final Buffer b;
    private final Semaforo s;
    private final JLabel productorImg;
    private final JLabel imgEstado;

    public Productor(Buffer b, Semaforo s, JLabel imgProdcutor, JLabel imgEstado) {
        this.b = b;
        this.s = s;
        this.productorImg = imgProdcutor;
        this.imgEstado = imgEstado;
        SwingUtilities.invokeLater(() -> {
            this.imgEstado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/sleep.gif")));
            this.imgEstado.setVisible(true);
            this.productorImg.setLocation(30, 280);
        });

    }

    @Override
    public void run() {

        while (true) {
            crearMuebles();
        }

    }

    private void crearMuebles() {

        //bloque sincronizado con los otros hilos
        synchronized (s.lock) {

            if (b.size() == 0) {// si el buffer esta vacio

                s.alternar(Semaforo.PRODUCIR);// cambiar color de semaforo
                SwingUtilities.invokeLater(() -> {

                    imgEstado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/process.gif")));
                });

                for (int i = 0; i < 4; i++) {// se agregan 4 productos
                    //sonido de madera
                    try {
                        URL defaultSound = getClass().getResource("/Sonidos/Sonido8.wav");
                        File soundFile = new File(defaultSound.toURI());
                        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(soundFile);
                        Clip clip = AudioSystem.getClip();
                        clip.open(audioInputStream);
                        clip.start();
                    } catch (IOException | URISyntaxException | LineUnavailableException | UnsupportedAudioFileException ex) {
                        JOptionPane.showMessageDialog(null, "Error ejecutando audio del productor: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    }

                    try {

                        sleep((long) ((Math.random() * ((5000 - 300) + 1)) + 300));//poner en espera
                    } catch (InterruptedException ex) {
                        JOptionPane.showMessageDialog(null, "Error sleep en Productor: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    }

                    animationRight();

                    int img = (int) ((Math.random() * ((5 - 1) + 1)) + 1);

                    b.add(new JLabel(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/producto" + img + ".png"))));
                    System.out.println("Agregando mueble...");

                    animationLeft();

                }

                s.lock.notifyAll();//notificar a todos los hilos para que despierten

            }

            SwingUtilities.invokeLater(() -> {
                imgEstado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/sleep.gif")));
            });
            try {
                s.lock.wait();// poner a este hilo en espera
            } catch (InterruptedException ex) {
                JOptionPane.showMessageDialog(null, "Error poniendo Productor en espera: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }

        }
    }

    private void animationRight() {
        SwingUtilities.invokeLater(() -> {

            imgEstado.setVisible(false);
        });

        for (int i = 30; i < 200; i++) {

            productorImg.setLocation(i, 280);

            try {

                sleep(3);//
            } catch (InterruptedException ex) {
                JOptionPane.showMessageDialog(null, "Error sleep en Productor: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }

        }

    }

    private void animationLeft() {

        for (int i = 200; i > 30; i--) {

            productorImg.setLocation(i, 280);

            try {

                sleep(3);//
            } catch (InterruptedException ex) {
                JOptionPane.showMessageDialog(null, "Error sleep en Productor: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }

        }

        SwingUtilities.invokeLater(() -> {

            imgEstado.setVisible(true);
        });
    }

}
