package com.umg.simulador.controlador;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 *
 * @author jaasiel17
 */
public class Semaforo {

    // constantes para cambiar estado del semaforo
    public static final int PRODUCIR = 1;
    public static final int CONSUMIR = 2;

    private final JLabel semP;
    private final JLabel semC;
    public Object lock = new Object();

    public Semaforo(JLabel semp, JLabel semc) {
        this.semP = semp;
        this.semC = semc;
        SwingUtilities.invokeLater(() -> {
            this.semP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/semaforo0.png")));
            this.semC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/semaforo0.png")));
        });
    }

    public void alternar(int estado) {

        SwingUtilities.invokeLater(() -> {

            try {
                URL defaultSound = getClass().getResource("/Sonidos/Sonido2.wav");
                File soundFile = new File(defaultSound.toURI());
                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(soundFile);
                Clip clip = AudioSystem.getClip();
                clip.open(audioInputStream);
                clip.start();
            } catch (IOException | URISyntaxException | LineUnavailableException | UnsupportedAudioFileException ex) {
                JOptionPane.showMessageDialog(null, "Error ejecutando audio del semaforo: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

            }

            switch (estado) {
                case 1://producir
                    semP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/semaforo1.png")));
                    semC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/semaforo0.png")));
                    System.out.println("Semaforo: producir");
                    break;

                case 2:// consumir
                    semP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/semaforo0.png")));
                    semC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/semaforo1.png")));
                    System.out.println("Semaforo: consumir");
                    break;

                default:
                    break;

            }

        });

    }

}
