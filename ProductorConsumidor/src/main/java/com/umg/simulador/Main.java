package com.umg.simulador;

import com.umg.simulador.controlador.ControladorPrincipal;
import com.umg.simulador.vista.Vista;
import javax.swing.SwingUtilities;

/**
 *
 * @author jaasiel17
 */
public class Main {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {

            Vista v = new Vista();
            ControladorPrincipal controller = new ControladorPrincipal(v);

        });
    }
}
