/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umg.simulador.vista;


 
public class Vista extends javax.swing.JFrame {
    
 
    public Vista() {
        initComponents();
        
    }
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanelImage1 = new com.umg.simulador.modelo.JPanelImageBackground();
        consumidor2 = new javax.swing.JLabel();
        consumidor1 = new javax.swing.JLabel();
        barraBuffer = new javax.swing.JProgressBar();
        productor = new javax.swing.JLabel();
        btnStop = new javax.swing.JButton();
        btnPlay = new javax.swing.JButton();
        semaforoC = new javax.swing.JLabel();
        lblEstado2 = new javax.swing.JLabel();
        lblEstado = new javax.swing.JLabel();
        lblEstado1 = new javax.swing.JLabel();
        semaforoP = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        lblEstado3 = new javax.swing.JLabel();
        pnlBuffer = new com.umg.simulador.modelo.JPanelImageBuffer();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1150, 720));
        setResizable(false);

        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanelImage1.setLayout(null);

        consumidor2.setForeground(new java.awt.Color(255, 255, 255));
        consumidor2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        consumidor2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/consumidor2.png"))); // NOI18N
        consumidor2.setText("Consumidor 2");
        consumidor2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        consumidor2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jPanelImage1.add(consumidor2);
        consumidor2.setBounds(950, 520, 128, 150);

        consumidor1.setForeground(new java.awt.Color(255, 255, 255));
        consumidor1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        consumidor1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/consumidor1.png"))); // NOI18N
        consumidor1.setText("Consumidor 1");
        consumidor1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        consumidor1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jPanelImage1.add(consumidor1);
        consumidor1.setBounds(940, 100, 128, 150);

        barraBuffer.setBackground(new java.awt.Color(255, 255, 255));
        barraBuffer.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        barraBuffer.setMaximum(4);
        barraBuffer.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        barraBuffer.setBorderPainted(false);
        barraBuffer.setStringPainted(true);
        jPanelImage1.add(barraBuffer);
        barraBuffer.setBounds(360, 460, 530, 30);

        productor.setBackground(new java.awt.Color(100, 100, 100));
        productor.setForeground(new java.awt.Color(255, 255, 255));
        productor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        productor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/carpintero.png"))); // NOI18N
        productor.setText("Productor");
        productor.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        productor.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jPanelImage1.add(productor);
        productor.setBounds(30, 280, 147, 155);

        btnStop.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnStop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icons8-stop-squared-48.png"))); // NOI18N
        btnStop.setBorder(null);
        btnStop.setBorderPainted(false);
        btnStop.setContentAreaFilled(false);
        btnStop.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnStop.setFocusPainted(false);
        btnStop.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnStop.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icons8-stop-squared-48.png"))); // NOI18N
        btnStop.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icons8-stopZoom.png"))); // NOI18N
        jPanelImage1.add(btnStop);
        btnStop.setBounds(620, 17, 80, 60);

        btnPlay.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnPlay.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icons8-circled-play-48.png"))); // NOI18N
        btnPlay.setBorder(null);
        btnPlay.setBorderPainted(false);
        btnPlay.setContentAreaFilled(false);
        btnPlay.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPlay.setFocusPainted(false);
        btnPlay.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPlay.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icons8-circled-play-48.png"))); // NOI18N
        btnPlay.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icons8-playZoom.png"))); // NOI18N
        jPanelImage1.add(btnPlay);
        btnPlay.setBounds(520, 17, 80, 60);

        semaforoC.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        semaforoC.setForeground(new java.awt.Color(255, 255, 255));
        semaforoC.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        semaforoC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/semaforo0.png"))); // NOI18N
        semaforoC.setText("Semáforo");
        semaforoC.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanelImage1.add(semaforoC);
        semaforoC.setBounds(810, 220, 90, 100);

        lblEstado2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblEstado2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/sleep.gif"))); // NOI18N
        jPanelImage1.add(lblEstado2);
        lblEstado2.setBounds(980, 440, 70, 60);

        lblEstado.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblEstado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/sleep.gif"))); // NOI18N
        jPanelImage1.add(lblEstado);
        lblEstado.setBounds(70, 210, 70, 60);

        lblEstado1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblEstado1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/sleep.gif"))); // NOI18N
        jPanelImage1.add(lblEstado1);
        lblEstado1.setBounds(970, 20, 70, 60);

        semaforoP.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        semaforoP.setForeground(new java.awt.Color(255, 255, 255));
        semaforoP.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        semaforoP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/semaforo0.png"))); // NOI18N
        semaforoP.setText("Semáforo");
        semaforoP.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanelImage1.add(semaforoP);
        semaforoP.setBounds(350, 220, 90, 100);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("BUFFER");
        jPanelImage1.add(jLabel1);
        jLabel1.setBounds(560, 490, 130, 22);

        lblEstado3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblEstado3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/process.gif"))); // NOI18N
        jPanelImage1.add(lblEstado3);
        lblEstado3.setBounds(970, 250, 70, 60);

        pnlBuffer.setOpaque(false);
        jPanelImage1.add(pnlBuffer);
        pnlBuffer.setBounds(350, 330, 550, 120);

        jPanel1.add(jPanelImage1, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1150, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 720, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JProgressBar barraBuffer;
    public javax.swing.JButton btnPlay;
    public javax.swing.JButton btnStop;
    public javax.swing.JLabel consumidor1;
    public javax.swing.JLabel consumidor2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private com.umg.simulador.modelo.JPanelImageBackground jPanelImage1;
    public javax.swing.JLabel lblEstado;
    public javax.swing.JLabel lblEstado1;
    public javax.swing.JLabel lblEstado2;
    public javax.swing.JLabel lblEstado3;
    public com.umg.simulador.modelo.JPanelImageBuffer pnlBuffer;
    public javax.swing.JLabel productor;
    public javax.swing.JLabel semaforoC;
    public javax.swing.JLabel semaforoP;
    // End of variables declaration//GEN-END:variables
}
