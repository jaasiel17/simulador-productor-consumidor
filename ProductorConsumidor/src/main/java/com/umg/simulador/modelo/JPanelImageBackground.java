package com.umg.simulador.modelo;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author jaasiel17
 */
public class JPanelImageBackground extends JPanel {

    private BufferedImage bf;

    public JPanelImageBackground() {
        try {
            bf = ImageIO.read(new File(getClass().getResource("/Imagenes/background.png").toURI()));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al cargar backgroung image " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(bf, 0, 0, this);
        
    }
    
    

}
