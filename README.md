# Simulador-Productor-consumidor

![Vista principal](Captura.PNG "Vista principal")

## Simulador de procesos hecho en Java 8

La idea para la solución es la siguiente, ambos procesos (productor y consumidor) 
se ejecutan simultáneamente y se “despiertan” o “duermen” según el estado del buffer. 
Concretamente, el productor agrega productos mientras quede espacio y en el momento en
 que se llene el buffer se pone a “dormir”. Cuando el consumidor toma un producto 
notifica al productor que puede comenzar a trabajar nuevamente. En caso contrario, si 
el buffer se vacía, el consumidor se pone a dormir y en el momento en que el productor 
agrega Un producto crea una señal para despertarlo. Se puede encontrar una solución usando 
mecanismos de comunicación de interprocesos, generalmente se usan semáforos.